-- Franchise
INSERT into franchise(franchise_desc, franchise_full_name) VALUES('En serie filmer om superhjältar etc', 'MARVEL');
INSERT into franchise(franchise_desc, franchise_full_name) VALUES('En serie filmer om drakar etc', 'LORD OF THE RINGS');
-- Movies
INSERT into movie(movie_director, movie_genre, movie_picture, movie_release, movie_title, movie_trailer, franchise_id) VALUES ('ELON', 'ACTION', 'WWW.google.com', 2001, 'IRONMAN', 'WWW.YOUTUBE.COM', 1);
INSERT into movie(movie_director, movie_genre, movie_picture, movie_release, movie_title, movie_trailer, franchise_id) VALUES ('ELON', 'ACTION', 'WWW.google.com', 2001, 'IRONMAN', 'WWW.YOUTUBE.COM', 2);
INSERT into movie(movie_director, movie_genre, movie_picture, movie_release, movie_title, movie_trailer, franchise_id) VALUES ('ELON', 'ACTION', 'WWW.google.com', 2001, 'IRONMAN', 'WWW.YOUTUBE.COM', 1);
-- Characters
INSERT into character(char_alias, char_gender, char_full_name, char_pic_url) VALUES ('BATMAN', 'MALE', 'WAYNE', 'WWW.GOOGLE.COM');
INSERT into character(char_alias, char_gender, char_full_name, char_pic_url) VALUES ('IRON MAN', 'MALE', 'ROBERT JR', 'WWW.GOOGLE.COM');
INSERT into character(char_alias, char_gender, char_full_name, char_pic_url) VALUES ('HULKEN', 'MALE', 'BRUCE BANNER', 'WWW.GOOGLE.COM');
-- Characters in movies
INSERT into movie_character(movie_id, character_id) VALUES(1, 1);
INSERT into movie_character(movie_id, character_id) VALUES(2, 1);
INSERT into movie_character(movie_id, character_id) VALUES(1, 2);
INSERT into movie_character(movie_id, character_id) VALUES(3, 3);
