package com.example.spring_api.service.franchise;

import com.example.spring_api.model.Character;
import com.example.spring_api.model.Franchise;
import com.example.spring_api.model.Movie;
import com.example.spring_api.service.CrudService;

import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Franchise updateMoviesInFranchise(Collection<Movie> newList, Integer id);
}
