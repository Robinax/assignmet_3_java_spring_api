package com.example.spring_api.service.franchise;
import com.example.spring_api.model.Franchise;
import com.example.spring_api.model.Movie;
import com.example.spring_api.repository.FranchiseRepository;
import com.example.spring_api.repository.MovieRepository;
import com.example.spring_api.service.exceptions.FranchiseNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private FranchiseRepository franchiseRepository;
    private MovieRepository movieRepository;
    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository){
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise updateMoviesInFranchise(Collection<Movie> newList, Integer id){
        var newMovieSet = newList.stream().collect(Collectors.toSet());
        Franchise franchise = franchiseRepository.findById(id).get();

        newMovieSet.forEach(movieInSet -> {
            var movieId = movieInSet.getId();
            Movie movie = movieRepository.findById(movieId).get();
            movie.setFranchise(franchise);
            movieRepository.save(movie);
        });
        franchise.setMovies(newMovieSet);

        return franchiseRepository.save(franchise);
    }

    @Override
    public void deleteById(Integer integer) {
        franchiseRepository.deleteById(integer);
    }
}
