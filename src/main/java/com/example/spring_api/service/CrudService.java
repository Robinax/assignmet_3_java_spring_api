package com.example.spring_api.service;

import java.util.Collection;
import java.util.List;

public interface CrudService<T, ID> {
    T findById(ID id);
    Collection<T> findAll();
    T add(T entity);
    T update(T entity);
    void deleteById(ID id);
}
