package com.example.spring_api.service.movie;

import com.example.spring_api.model.Character;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.service.CrudService;

import java.util.Collection;
import java.util.Set;


public interface MovieService extends CrudService<Movie, Integer> {
    Movie updateCharactersInMovie(Collection<Character> newList, Integer id);
}
