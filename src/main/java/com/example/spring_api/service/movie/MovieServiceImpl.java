package com.example.spring_api.service.movie;
import com.example.spring_api.mappers.CharacterMapper;
import com.example.spring_api.model.Character;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.repository.CharacterRepository;
import com.example.spring_api.repository.MovieRepository;
import com.example.spring_api.service.exceptions.FranchiseNotFoundException;
import com.example.spring_api.service.exceptions.MovieNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService{

    private MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie updateCharactersInMovie(Collection<Character> newList, Integer id){
        var newCharacterSet = newList.stream().collect(Collectors.toSet());
        Movie movie = movieRepository.findById(id).get();

        movie.setCharacters(newCharacterSet);

        return movieRepository.save(movie);
    }
    @Override
    public void deleteById(Integer integer) {
        if(movieRepository.existsById(integer)){
            Movie movie = movieRepository.findById(integer).get();
            movie.getCharacters().forEach(c -> {
                c.getMovies().remove(movie);
            });
            movieRepository.deleteById(integer);
        } else {
            System.out.println("FUNKAR INTE");
        }
    }

}
