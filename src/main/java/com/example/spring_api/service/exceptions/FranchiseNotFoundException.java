package com.example.spring_api.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchiseNotFoundException extends RuntimeException{
    public FranchiseNotFoundException(int id){super("No franchise exist with id: " + id);}
}
