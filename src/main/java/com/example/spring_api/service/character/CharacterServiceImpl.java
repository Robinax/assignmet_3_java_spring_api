package com.example.spring_api.service.character;

import com.example.spring_api.model.Character;
import com.example.spring_api.repository.CharacterRepository;
import com.example.spring_api.service.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class CharacterServiceImpl implements CharacterService{

    private CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        characterRepository.deleteById(integer);
    }
}
