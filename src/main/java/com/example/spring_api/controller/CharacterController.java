package com.example.spring_api.controller;

import com.example.spring_api.mappers.CharacterMapper;
import com.example.spring_api.mappers.MovieMapper;
import com.example.spring_api.model.Character;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.model.dto.movie.MovieDTO;
import com.example.spring_api.service.character.CharacterService;
import com.example.spring_api.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/character")
@Tag(name="Character Endpoints")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;
    private final MovieMapper movieMapper;
    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, MovieMapper movieMapper){
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Get a movie character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}") //GET: localhost:8080/api/v1/character/{ID}
    public ResponseEntity findById(@PathVariable int id){
        CharacterDTO character = characterMapper.characterToCharacterDto(
                characterService.findById(id)
        );
        return ResponseEntity.ok(character);
    }

    @Operation(summary = "Get a character by movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}/movies") //GET: localhost:8080/api/v1/character/{ID}/movies
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<MovieDTO> moviesById = movieMapper.movieToMovieDto(
                characterService.findById(id).getMovies()
        );
        return ResponseEntity.ok(moviesById);
    }

    @Operation(summary = "Get all movie characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping //GET: localhost:8080/api/v1/character/
    public ResponseEntity findAll(){
        Collection<CharacterDTO> characters = characterMapper.characterToCharacterDto(
                characterService.findAll()
        );
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Add a new character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "New character added",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Failed to add character",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @PostMapping //POST: localhost:8080/api/v1/character/{ID}
    public ResponseEntity add(@RequestBody CharacterDTO characterDTO){
        Character chara = characterService.add(
                characterMapper.characterDtoToCharacter(characterDTO)
        );

        URI location = URI.create("character/" + chara.getId());
        return ResponseEntity.created(location).build();
    }
    @Operation(summary = "Update an existing character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("{id}") //PUT: localhost:8080/api/v1/character/{ID}
    public ResponseEntity update(@RequestBody CharacterDTO characterDto, @PathVariable int id){
        if(id != characterDto.getId()) return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterDtoToCharacter(characterDto)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete an existing character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") //DELETE: localhost:8080/api/v1/character/{ID}
    public ResponseEntity delete(@PathVariable int id){
        Set<Movie> movies = characterService.findById(id).getMovies();

        movies.forEach(movie -> {
            var characters = movie.getCharacters();
            Set<Character> newCharList = new HashSet<>();
            characters.forEach(character -> {
                if (character != characterService.findById(id)) newCharList.add(character);
            });
            movie.setCharacters(newCharList);
        });

        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
