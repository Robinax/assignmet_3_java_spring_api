package com.example.spring_api.controller;

import com.example.spring_api.mappers.CharacterMapper;
import com.example.spring_api.mappers.MovieMapper;
import com.example.spring_api.model.Character;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.model.dto.movie.MovieDTO;
import com.example.spring_api.model.dto.movie.UpdateCharactersInMovieDTO;
import com.example.spring_api.service.character.CharacterService;
import com.example.spring_api.service.franchise.FranchiseService;
import com.example.spring_api.service.movie.MovieService;
import com.example.spring_api.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@RestController
@RequestMapping(path = "api/v1/movies")
@Tag(name="Movie Endpoints")
public class MovieController {

    private final MovieService movieService;
    private final CharacterService characterService;
    private final FranchiseService franchiseService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper, CharacterService characterService, FranchiseService franchiseService){
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
        this.characterService = characterService;
        this.franchiseService = franchiseService;
    }

    @Operation(summary = "Get a movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No movie with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("/{id}") //GET: localhost:8080/api/v1/movies/{ID}
    public ResponseEntity findById(@PathVariable int id){
        MovieDTO movie = movieMapper.movieToMovieDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }

    @Operation(summary = "Get all characters in a movie using the movieID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class)) }), //NEED NEW DTO!
            @ApiResponse(responseCode = "404",
                    description = "No movie with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("/{id}/characters") //GET: localhost:8080/api/v1/movies/{ID}/characters
    public ResponseEntity findCharacterWithIdInMovie(@PathVariable int id){

        Collection<CharacterDTO> characterById = characterMapper.characterToCharacterDto(
                movieService.findById(id).getCharacters()
        );
        return ResponseEntity.ok(characterById);
    }

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No movie with that ID exists",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping //GET: localhost:8080/api/v1/movie/
    public ResponseEntity findAll(){
        Collection<MovieDTO> movies = movieMapper.movieToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movies);
    }


    @Operation(summary = "Update an existing character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/updateCharacters") //PUT: localhost:8080/api/v1/movies/{ID}/updateCharacters
    public ResponseEntity updateCharacterInMovie(@RequestBody UpdateCharactersInMovieDTO updateCharactersInMovieDTO, @PathVariable int id){
        Collection<Character> collection = new HashSet<>();
        var characterArray = updateCharactersInMovieDTO.getCharacters();
        //Loop through the characters from array and add all the characters into a collection.
        for (int i = 0; i < characterArray.length; i++) {
            var character = characterService.findById(characterArray[i]);
            collection.add(character);
        }

        movieService.updateCharactersInMovie(collection, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Add a new movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "New movie added",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Failed to add movie",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping //POST: localhost:8080/api/v1/movie/{ID}
    public ResponseEntity add(@RequestBody MovieDTO movieDTO){
        Movie mov = movieService.add(movieMapper.movieDtoToMovie(movieDTO));

        URI location = URI.create("movies/" + mov.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update an existing movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") //PUT: localhost:8080/api/v1/movies/{ID}
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable int id){

        MovieDTO movie = movieMapper.movieToMovieDto(movieService.findById(id));
        var currentFranchise = movie.getFranchise();
        movieDTO.setFranchise(currentFranchise);

        movieService.update(
                movieMapper.movieDtoToMovie(movieDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete an existing movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") //DELETE: localhost:8080/api/v1/movies/{ID}
    public ResponseEntity delete(@PathVariable int id){
        var characters = movieService.findById(id).getCharacters();
        characters.forEach(character -> {
            var movies = character.getMovies();
            Set<Movie> newMovieList = new HashSet<>();
            movies.forEach(movie -> {
                if (movie != movieService.findById(id)) newMovieList.add(movie);
            });
            character.setMovies(newMovieList);
        });
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
