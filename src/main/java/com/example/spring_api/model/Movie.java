package com.example.spring_api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name= "franchise_id")
    private Franchise franchise;

    @Column(name = "movie_title", length = 40, nullable = false)
    private String title;
    @Column(name = "movie_genre", length = 80, nullable = false)
    private String genre;
    @Column(name = "movie_release", length = 40, nullable = false)
    private Integer release;
    @Column(name = "movie_director", length = 40, nullable = false)
    private String director;
    @Column(name = "movie_picture", length = 200, nullable = false)
    private String pictureUrl;
    @Column(name = "movie_trailer", length = 200, nullable = false)
    private String trailerUrl;
}
