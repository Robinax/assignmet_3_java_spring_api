package com.example.spring_api.model.dto.franchiseDTO;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;


@Data
public class FranchiseDTO {
    @NotEmpty
    @Size(min = 1, message = "Id should be the same as in the URL")
    private int id;
    private Set<Integer> movies;
    @NotEmpty
    @Size(min = 5, message = "The name should be at least 5 letters")
    private String name;
    @NotEmpty
    @Size(min = 15, message = "The description should be at least 15 letters")
    private String description;
}
