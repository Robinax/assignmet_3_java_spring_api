package com.example.spring_api.model.dto.franchiseDTO;
import lombok.Data;

@Data
public class UpdateFranchiseMovieListDTO {
    private int[] movies;
}

