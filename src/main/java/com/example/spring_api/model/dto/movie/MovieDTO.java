package com.example.spring_api.model.dto.movie;

import com.example.spring_api.model.dto.character.CharacterDTO;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Set;

@Data
public class MovieDTO {
    @NotEmpty
    @Size(min = 1, message = "Id should be the same as in the URL")
    private int id;
    private Set<Integer> characters;
    @NotEmpty
    @Size(min = 5, message = "The title should be at least 10 letters")
    private String title;
    @NotEmpty
    @Size(min = 5, message = "The genre should be at least 10 letters")
    private String genre;
    @NotEmpty
    @Size(min = 4, message = "The release should be at least 4 numbers")
    private int release;
    @NotEmpty
    @Size(min = 5, message = "The director should be at least 5 letters")
    private String director;
    @NotEmpty
    @Size(min = 10, message = "The picture Url should be at least 10 letters")
    private String pictureUrl;
    @NotEmpty
    @Size(min = 10, message = "The trailer Url should be at least 10 letters")
    private String trailerUrl;
    private int franchise;
}
