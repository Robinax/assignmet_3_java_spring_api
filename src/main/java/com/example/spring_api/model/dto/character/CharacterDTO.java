package com.example.spring_api.model.dto.character;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class CharacterDTO {
    @NotEmpty
    @Size(min = 1, message = "Id should be the same as in the URL")
    private int id;
    @NotEmpty
    @Size(min = 5, message = "name should have at least 5 letters")
    private String name;
    @NotEmpty
    @Size(min = 5, message = "alias should have at least 5 letters")
    private String alias;
    @NotEmpty
    @Size(min = 4, message = "gender should be at least 4 letters")
    private String gender;
    @NotEmpty
    @Size(min = 10, message = "The picture Url should be at least 10 letters")
    private String pictureUrl;
    private Set<Integer> movies;
}
