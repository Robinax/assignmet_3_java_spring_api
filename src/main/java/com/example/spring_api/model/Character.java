package com.example.spring_api.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;

    @Column(name = "char_fullName", length = 40, nullable = false)
    private String name;
    @Column(name = "char_alias", length = 40, nullable = false)
    private String alias;
    @Column(name = "char_gender", length = 20, nullable = false)
    private String gender;
    @Column(name = "char_picUrl", length = 70, nullable = false)
    private String pictureUrl;
}
