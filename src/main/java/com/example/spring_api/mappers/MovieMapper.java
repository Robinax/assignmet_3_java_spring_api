package com.example.spring_api.mappers;

import com.example.spring_api.model.Character;
import com.example.spring_api.model.Franchise;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.model.dto.movie.MovieDTO;
import com.example.spring_api.service.character.CharacterService;
import com.example.spring_api.service.franchise.FranchiseService;
import com.example.spring_api.service.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdToCharacter")
    public abstract Movie movieDtoToMovie(MovieDTO dto);

    public abstract Collection<Movie> movieDtoToMovie(Collection<MovieDTO> movies);

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(int id){return franchiseService.findById(id);}

    @Named("characterIdToCharacter")
    Character mapIdToCharacter(int id){return characterService.findById(id);}

    @Named("charactersToIds")
    Set<Integer> mapCharacterToIds(Set<Character> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
}
