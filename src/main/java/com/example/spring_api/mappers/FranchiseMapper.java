package com.example.spring_api.mappers;

import com.example.spring_api.model.Character;
import com.example.spring_api.model.Franchise;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.model.dto.franchiseDTO.FranchiseDTO;
import com.example.spring_api.service.franchise.FranchiseService;
import com.example.spring_api.service.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchise);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO dto);

    public abstract Collection<Franchise> franchiseDtoToFranchise(Collection<FranchiseDTO> franchise);

    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id){return movieService.findById(id);}

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
}
