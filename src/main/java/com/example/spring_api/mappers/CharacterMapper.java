package com.example.spring_api.mappers;

import com.example.spring_api.model.Character;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.service.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);

    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Character characterDtoToCharacter(CharacterDTO dto);

    public abstract Collection<Character> characterDtoToCharacter(Collection<CharacterDTO> characters);

    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id){return movieService.findById(id);}

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
}
