# 👏 Movie web API - A Java & PostgresSQL application

This is an application to run API endpoints. 

## ✌️ Description

### 🔥 About the application

This application creates a number of endpoints to a movie API. The application has endpoints in three categories: Movie, Character and Franchise.

For this project you will have to setup following environments:

* PostgreSQL (E.g. PgAdmin)


#### 🔥 Application endpoints

* Movie
  * **GET**: All movies ```/api/v1/movie/```
  * **GET**: Movie by ID ```/api/v1/movie/{Id}```
  * **GET**: Characters in a movie ```/api/v1/movie/{ID}/characters```
  * **POST**: Add new movie ```/api/v1/movie/{ID}```
  * **PUT**: Update character in a movie ```api/v1/movie/{ID}/updateCharacters```
  * **PUT**: Update movie ```/api/v1/movie/{ID}```
  * **DELETE**: Delete a movie ```/api/v1/movie/{ID}```
  

* Character
  * **GET**: All characters ```/api/v1/character/```
  * **GET**: Character by ID ```/api/v1/character/{ID}```
  * **GET**: Movies character attending in ```/api/v1/character/{ID}/movies```
  * **POST**: Add new character ```/api/v1/character/{ID}```
  * **PUT**: Update character ```/api/v1/character/{ID}```
  * **DELETE**: Delete a character ```/api/v1/character/{ID}```


* Franchise
  * **GET**: All franchises ```/api/v1/franchise```
  * **GET**: Franchise by ID ```/api/v1/franchise/{ID}```
  * **GET**: Characters in a franchise ```/api/v1/franchise/{ID}/characters```
  * **POST**: Add new franchise ```/api/v1/franchise{ID}```
  * **PUT**: Update franchise ```/api/v1/franchise/{ID}```
  * **PUT**: Update movie in a franchise ```/api/v1/franchise/{ID}/updateMovie```
  * **DELETE**: Delete a franchise ```/api/v1/franchise/{ID}```

### 🤖 Technologies

Tools and technologies used in this project.

* Java
* Spring Boot
* Spring Web
* Spring Data JPA
* PostgresSQL
* PG Admin
* Docker
* OpenAPI / Swagger
* Heroku
* Intellij
* Git Bash

### ⚠ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work.

### ⚡ Installing & Running

#### 🦾 Install

Follow the steps to clone the application.

1. Clone the repo:
```
git clone https://gitlab.com/elon.mansson/assignmet_3_java_spring_api.git
``` 

#### 💻 Executing program

How to run the application:
```
Build Project - SpringApiApplication.
Run
```

## 😎 Authors

Contributors and contact information

Elon Månsson
[@elon.mansson](https://gitlab.com/elon.mansson/)

Philip Hjelmberg
[@PhilipHjelmberg](https://gitlab.com/PhilipHjelmberg)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
